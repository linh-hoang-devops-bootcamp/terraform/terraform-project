#vpc variables
variable "vpc_cidr" {
  default       = "10.0.0.0/16"
  description   = "vpc cidr block"
  type          = string
}

variable "public_subnet_az1_cidr" {
  default       = "10.0.0.0/24"
  description   = "public az1 cidr block"
  type          = string

}

variable "public_subnet_az2_cidr" {
  default       = "10.0.1.0/24"
  description   = "public az2 cidr block"
  type          = string
}

variable "private_app_subnet_az1_cidr" {
  default       = "10.0.2.0/24"
  description   = "private app subnet az1 cidr block"
  type          = string
}

variable "private_app_subnet_az2_cidr" {
  default       = "10.0.3.0/24"
  description   = "private app subnet az2 cidr block"
  type          = string
}

variable "private_data_subnet_az1_cidr" {
  default       = "10.0.4.0/24"
  description   = "private data subnet az1 cidr block"
  type          = string
}

variable "private_data_subnet_az2_cidr" {
  default       = "10.0.5.0/24"
  description   = "private data subnet az2 cidr block"
  type          = string
}

#Security Group variables
variable "ssh_location" {
  default       = "0.0.0.0/0"
  description   = "the ip adress can ssh into webserver"
  type          = string
}

#rds variables
variable "database_snapshot_identifier" {
  default       = "arn:aws:rds:us-east-1:669089518573:snapshot:flletcart-final-snapshot"
  description   = "the databse snapshot arn"
  type          = string
}

variable "database_instance_class" {
  default       = "db.t2.micro"
  description   = "the databse instance type"
  type          = string
}

variable "database_instance_identifier" {
  default       = "dev-rds-db"
  description   = "the databse instance identifier"
  type          = string
}

variable "database_subnet_group_name" {
  default       = "dev-rds-db"
  description   = "the databse instance identifier"
  type          = string
}

variable "multi_az_deployment" {
  default       = false
  description   = "created a standy db instance"
  type          = bool
}

#application load balancer variables
variable "ssl_certificate_arn" {
  default = "arn:aws:acm:us-east-1:669089518573:certificate/e78608f0-513d-49f7-980c-4c2a39bd4601"
  description = "ssl certificate arn"
  type = string
}

#sns topic variable
variable "operator_email" {
  default = "hoanglinh1949@gmail.com"
  description = "a valid email address"
  type = string
}

#auto scaling group variables
variable "launch_template_name" {
  default = "dev-launch-template"
  description = "name of the template"
  type = string
}

variable "ec2_image_id" {
  default = "ami-036d579be44b9a37b"
  description = "name of the template"
  type = string
}

variable "ec2_instance_type" {
  default = "t2.micro"
  description = "the ec2 instance type"
  type = string
}

variable "ec2_key_pair_name" {
  default = "myec2key"
  description = "name of the ec2 key pair"
  type = string
}
